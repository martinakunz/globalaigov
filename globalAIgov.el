(require 'ox-publish)

(defvar globalAIgov-website-html-preamble
  "<header>
<h1>Global AI Governance</h1>
</header>
<nav>
<ul>
<li><a href='/'>Home</a></li>
<li><a href='/'>Themes</a>
<ul>
<li><a href='/themes/transport.html'>AI & Transport</a>
<ul>
<li><a href='/themes/aviation.html'>Aviation</a></li>
<li><a href='/themes/shipping.html'>Shipping</a></li>
<li><a href='/themes/groundtransport.html'>Ground Transport</a></li>
</ul>
</li>
<li><a href='/themes/crime.html'>AI & Crime</a></li>
</ul>
</li>
<li><a href='/'>Tools</a>
<ul>
<li><a href='/tools/treaties/'>Treaties</a></li>
<li><a href='/tools/standards.html'>Voluntary Standards</a></li>
<li><a href='/tools/globalgoals.html'>Global Goals</a></li>
</ul>
</li>
<li><a href='/participants/'>Participants</a>
<ul>
<li><a href='/participants/states.html'>States</a>
<ul>
<li><a href='/participants/china.html'>China</a></li>
<li><a href='/participants/usa.html'>United States</a></li>
</ul>
</li>
<li><a href='/participants/igos.html'>Intergovernmental Organizations</a></li>
<li><a href='/participants/fora.html'>Multistakeholder Fora</a></li>
</ul>
</li>
</ul>
</nav>")

(defvar globalAIgov-website-html-postamble
 (concat "<footer>
<div class='container'><div class='item'>
Copyright © 2020–2023 CC-BY 4.0 %a<br>
Last updated " (format-time-string "%Y-%m-%d") " <br>
Built with %c <br>
Source code shared under <a href='https://www.gnu.org/licenses/agpl-3.0.html'>AGPL</a> on <a href='https://gitlab.com/martinakunz/globalaigov'>Gitlab</a> <br></div>
<div class='item'><a href='https://gitlab.com/martinakunz/globalaigov'><img src='/img/gitlab-logo-700.png' height='90' class='logo'/></a>
<a href='https:/d3js.org'><img src='/img/d3-white.png' height='50' class='logo'/></a></div>
</div>
<p> The information provided by this website is for general informational and educational purposes only and is not a substitute for professional legal advice. It is work in progress and necessarily incomplete.
State borders and denominations do not imply endorsement. Maps use <a href='https://www.naturalearthdata.com/downloads/50m-cultural-vectors/50m-admin-0-details/'>Natural Earth</a> data. More info <a href='https://gitlab.com/legalinformatics/treaty-participation-maps'>here</a>.</p>
<p> Work on this website was supported by Cambridge University's Leverhulme Centre for the Future of Intelligence (<a href='http://lcfi.ac.uk/'>LCFI</a>), under Leverhulme Trust Grant RC-2015-067.</p>
</footer>"))


(setq org-publish-project-alist
      `(("globalAIgov-org"
         :base-directory "~/git/globalAIgov/org/"
         :base-extension "org"
         :publishing-directory "~/git/globalAIgov/public/"
	 :recursive t
         :publishing-function org-html-publish-to-html
         :section-numbers nil
         :with-toc t
	 :style-include-default nil
         :html-preamble ,globalAIgov-website-html-preamble
         :html-postamble ,globalAIgov-website-html-postamble
	 )
        ("globalAIgov" :components ("globalAIgov-org"))
       )
      )

;; C-h v
