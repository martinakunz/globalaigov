#+SETUPFILE: ../setup.org
#+TITLE: Global AI Governance and States

* Global AI governance and states
Needless to say, global AI governance rests fundamentally on the consent of nation states. Hence, political will to negotiate, adopt, ratify, finance, and implement international agreements is crucial. Even in the case of voluntary technical standards elaborated by NGOs with global impact such as ISO, IEEE and W3C, states' willingness to accept and support such standards by referring to them in law or by abstaining to regulate on the matter is what permits such standardization to flourish.

Consequently, this section of the website presents data on how supportive various states are and have been of relevant international organizations, treaties, standardization and other governance and policy efforts. 
# Participation and contributions to regional or restricted organizations and instruments will be used to 

Currently the following states are covered:

- [[./china.org][China]]
- [[./usa.org][United States of America]]

This is still work in progress and more states will be added in the future.

# Some people might worry about loss of national sovereignty when hearing of "global governance" or "international law" on AI, or 
# or need for world government.

# + states with special status under intl treaties (e.g. P5, headquarters)
# + states which have ratified the most treaties (e.g. top 10, for treaties listed in tools/treaties/)
# + global/regional headquarter states (ranked or not)
