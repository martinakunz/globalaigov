<!DOCTYPE html>
<html lang="en">
<head>
<!-- 2023-02-22 Wed 17:22 -->
<meta charset="utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1" />
<title>Software</title>
<meta name="author" content="Martina Kunz" />
<meta name="description" content="Demystifying algorithms, programming and AI applications for people working or interested in international law and policy." />
<meta name="keywords" content="algorithms, software, programming, artificial intelligence, international law" />
<meta name="generator" content="Org Mode" />
<link rel="stylesheet" href="/css/aigov.css" />
<link rel="icon" type="image/png" href="/img/favicon.png">
<script src="/js/d3.v7.min.js"></script>
<script src="/js/d3-geo.v3.min.js"></script>
<script>window.addEventListener('load', function addfntooltips() {var refs = document.getElementsByClassName("footref"); var defs = document.querySelectorAll("p.footpara"); var i, strl = []; for (i = 0; i < refs.length; i++) {strl[i] = defs[i].innerHTML; refs[i].insertAdjacentHTML("beforeend","<span class='fntooltip'>" + strl[i] + "</span>");}}, false);</script>
<script> window.goatcounter={path: function(p) {return location.host + p}} </script>
<script data-goatcounter="https://ahuman.goatcounter.com/count" async src="//gc.zgo.at/count.js"></script>
<noscript> <img src="https://ahuman.goatcounter.com/count?p=/test-noscript"></noscript>
</head>
<body>
<div id="org-div-home-and-up">
 <a accesskey="h" href=""> UP </a>
 |
 <a accesskey="H" href="/"> HOME </a>
</div><div id="preamble" class="status">
<header>
<h1>Global AI Governance</h1>
</header>
<nav>
<ul>
<li><a href='/'>Home</a></li>
<li><a href='/'>Themes</a>
<ul>
<li><a href='/themes/transport.html'>AI & Transport</a>
<ul>
<li><a href='/themes/aviation.html'>Aviation</a></li>
<li><a href='/themes/shipping.html'>Shipping</a></li>
<li><a href='/themes/groundtransport.html'>Ground Transport</a></li>
</ul>
</li>
<li><a href='/themes/crime.html'>AI & Crime</a></li>
</ul>
</li>
<li><a href='/'>Tools</a>
<ul>
<li><a href='/tools/treaties/'>Treaties</a></li>
<li><a href='/tools/standards.html'>Voluntary Standards</a></li>
<li><a href='/tools/globalgoals.html'>Global Goals</a></li>
</ul>
</li>
<li><a href='/participants/'>Participants</a>
<ul>
<li><a href='/participants/states.html'>States</a>
<ul>
<li><a href='/participants/china.html'>China</a></li>
<li><a href='/participants/usa.html'>United States</a></li>
</ul>
</li>
<li><a href='/participants/igos.html'>Intergovernmental Organizations</a></li>
<li><a href='/participants/fora.html'>Multistakeholder Fora</a></li>
</ul>
</li>
</ul>
</nav>
</div>
<div id="content" class="content">
<nav id="table-of-contents" role="doc-toc">
<h2>Table of Contents</h2>
<div id="text-table-of-contents" role="doc-toc">
<ul>
<li><a href="#org1928431">Demystifying algorithms and AI</a></li>
</ul>
</div>
</nav>

<div id="outline-container-org1928431" class="outline-2">
<h2 id="org1928431">Demystifying algorithms and AI</h2>
<div class="outline-text-2" id="text-org1928431">
<p>
Algorithms and their applications are sometimes shrouded in unnecessary mystery. Let&rsquo;s take an example from international law to illustrate some of the relevant concepts, tools and techniques.
</p>

<p>
Article 49 of the UN Convention on the Rights of the Child (<a href="https://www.ohchr.org/en/professionalinterest/pages/crc.aspx">CRC</a>) specifies its <a href="../tools/treaties/index.html#org9ade32b">entry into force</a> (EIF) as follows:
</p>

<blockquote>
<ol class="org-ol">
<li>The present Convention shall enter into force on the thirtieth day following the date of deposit with the Secretary-General of the United Nations of the twentieth instrument of ratification or accession.</li>

<li>For each State ratifying or acceding to the Convention after the deposit of the twentieth instrument of ratification or accession, the Convention shall enter into force on the thirtieth day after the deposit by such State of its instrument of ratification or accession.</li>
</ol>
</blockquote>

<p>
Now imagine a person presented with a list of participants and the dates of deposit of their instruments of ratification or accession to this Convention, tasked with calculating the entry into force date for each of them. What would the procedure be? It would go roughly like this:
</p>

<ol class="org-ol">
<li>Sort the list by date in ascending order.</li>
<li>Find the date of deposit of the twentieth instrument of consent to be bound (cut-off date).</li>
<li>Establish the date of general entry into force of the convention by adding thirty days to the date of deposit of the twentieth instrument of consent to be bound.</li>
<li>For all participants having deposited their instrument of consent to be bound by the cut-off date, set their individual entry into force date equal to the treaty&rsquo;s general entry into force date.</li>
<li>For all other participants, calculate their entry into force date by adding thirty days to the date of deposit of their instrument of consent to be bound.</li>
</ol>

<p>
This is basically an <b>algorithm</b> in natural language, i.e. a step-by-step instruction to perform a task, although the formulation here is a bit too specific to this treaty.<sup><a id="fnr.1" class="footref" href="#fn.1" role="doc-backlink">1</a></sup> Also, if the number of entries is low enough, for instance a few dozen, the person in question might do mental calculations without the need for additional tools. In this case the treaty has 196 parties (basically all states in the world except for the US), some of which may have joined during leap years, so a programmatic solution would likely be faster and more accurate.
Any general purpose programming language could be used to implement this kind of algorithm, the main difference being how many lines of code are needed, depending among others on in-built functions and third party modules.<sup><a id="fnr.2" class="footref" href="#fn.2" role="doc-backlink">2</a></sup>
</p>

<p>
Here is a solution in the <a href="https://www.python.org/"><b>Python</b></a> programming language, using third party data science libraries <a href="https://numpy.org/">numpy</a> and <a href="https://pandas.pydata.org/">pandas</a>, taking as input a spreadsheet in <a href="https://en.wikipedia.org/wiki/Comma-separated_values">csv format</a><sup><a id="fnr.3" class="footref" href="#fn.3" role="doc-backlink">3</a></sup> with columns <code>Participant</code> and <code>Consent</code>, and saving the output in the same format with an additional column for the individual entry into force date:
</p>

<div class="org-src-container">
<pre class="src src-python"><span style="color: #20b2aa; font-weight: bold;">import</span> numpy <span style="color: #20b2aa; font-weight: bold;">as</span> np
<span style="color: #20b2aa; font-weight: bold;">import</span> pandas <span style="color: #20b2aa; font-weight: bold;">as</span> pd
<span style="color: #cd853f;"># </span><span style="color: #cd853f;">loading data into dataframe</span>
<span style="color: #9acd32;">treatydata</span> = pd.read_csv(<span style="color: #ffa07a;">'CRCdata.csv'</span>, encoding=<span style="color: #ffa07a;">'utf-8'</span>, parse_dates=[<span style="color: #ffa07a;">'Consent'</span>], infer_datetime_format=<span style="color: #1e90ff;">True</span>)
<span style="color: #cd853f;"># </span><span style="color: #cd853f;">sort table by consent date</span>
<span style="color: #9acd32;">treatydata</span> = treatydata.sort_values(by=<span style="color: #ffa07a;">'Consent'</span>).reset_index(drop=<span style="color: #1e90ff;">True</span>)
<span style="color: #cd853f;"># </span><span style="color: #cd853f;">calculate general EIF date</span>
<span style="color: #9acd32;">treatyEIF</span> = treatydata.Consent[19] + pd.Timedelta(<span style="color: #ffa07a;">'30 days'</span>)
<span style="color: #cd853f;"># </span><span style="color: #cd853f;">calculate individual EIF dates</span>
<span style="color: #9acd32;">treatydata</span>[<span style="color: #ffa07a;">'EIF'</span>] = treatydata.Consent.<span style="color: #76ee00;">apply</span>(<span style="color: #20b2aa; font-weight: bold;">lambda</span> x: x + pd.Timedelta(<span style="color: #ffa07a;">'30 days'</span>) <span style="color: #20b2aa; font-weight: bold;">if</span> ((x + pd.Timedelta(<span style="color: #ffa07a;">'30 days'</span>)) &gt; treatyEIF) <span style="color: #20b2aa; font-weight: bold;">else</span> treatyEIF)
<span style="color: #cd853f;"># </span><span style="color: #cd853f;">saving updated dataframe to file</span>
treatydata.to_csv(<span style="color: #ffa07a;">'CRCdataEIF.csv'</span>, encoding=<span style="color: #ffa07a;">'utf-8'</span>, index=<span style="color: #1e90ff;">False</span>)
</pre>
</div>

<p>
As you can see, apart from imports and loading/saving the data, there are only three lines of code, but this could be reduced or lengthened depending on perceived readability and aesthetics.<sup><a id="fnr.4" class="footref" href="#fn.4" role="doc-backlink">4</a></sup> Lines prepended by the hash sign (#) are comments and don&rsquo;t count because they don&rsquo;t affect the outcome. This code block could be run in an interactive Python interpreter session or saved to file as a script and run on the command line of any computer with python, python-numpy and python-pandas installed.<sup><a id="fnr.5" class="footref" href="#fn.5" role="doc-backlink">5</a></sup>
</p>

<p>
A few clarifications:
</p>
<ul class="org-ul">
<li><code>19</code> is not a typo, in Python and many other programming languages, numbers start at 0, thus <code>treatydata.Consent[19]</code> is the date of the 20th instrument of consent</li>
<li>Pandas&rsquo; time series functionalities handle different lengths of months under the hood (see <a href="https://pandas.pydata.org/pandas-docs/stable/user_guide/timeseries.html">guide</a> if interested)</li>
<li>The lambda expression in <a href="https://pandas.pydata.org/pandas-docs/stable/reference/api/pandas.Series.apply.html"><code>.apply()</code></a> passes an anonymous <i>ad hoc</i> function, which could be defined separately as a custom function for re-use</li>
</ul>

<p>
Extending this program to work for any treaty with similar rules would be easy if treaties or relevant parts of it were published in a structured, machine readable format. Another option would be to provide entry into force conditions as user input when running the program in a desktop, mobile or web application. In practice, however, it would not be worth packaging this little program into an app because it only needs to be run once per treaty, thus an up-to-date public database with the end result and is a more efficient solution. 
</p>

<p>
Reality has not quite reached this optimal solution yet. Even <a href="https://treaties.un.org/Pages/Content.aspx?path=DB/UNTS/pageIntro_en.xml">UNTS online</a>, the most comprehensive database of contemporary treaties, is incomplete and out-of-date with regard to some major agreements. For instance, for the treaty participation <a href="aviation.html#org535b8c0">map</a> of the Hague Hijacking Convention I had to complement UNTS data with ICAO data and calculate missing EIF dates automatically with a similar script to the one discussed here. Sharing and explaining data processing scripts so that anyone can use and adapt them could help bridge the gap between the current and the optimal situation without the need for intermediary application development.
</p>

<p>
This example may seem to have little to do with AI, but that&rsquo;s only because the current hype focuses mostly on machine learning (ML). In fact, the sample code above uses several advances in <b>symbolic AI</b> from the past century which have become so widespread that they are part of the normal IT toolkit (list sorting algorithms, spreadsheet formats, programming logic etc.). Moreover, under AI definitions which take performance of cognitive tasks by a machine as the key criterion, this kind of application would arguably qualify. Still, only people engaged in fundraising or AI research would be likely to call this kind of application &rsquo;AI-based&rsquo; or &rsquo;smart&rsquo;. Also note that apps based on these techniques can be entirely transparent and self-explanatory if open sourced and not deliberately obfuscated.
</p>

<p>
Okay, so what could ML be used for in this context? One obvious idea might be to use it to extract the relevant pieces of information from agreement texts and pass them to the EIF calculation function. However, there is too much diversity and too little data. The texts of all treaties ever adopted probably amount to a fraction of social media posts published per day. Moreover, given the need for high accuracy when doing anything useful with legal information, ML would simply not be the best tool for this task.<sup><a id="fnr.6" class="footref" href="#fn.6" role="doc-backlink">6</a></sup>
</p>

<p>
A more ML-appropriate task would be to try to infer the EIF rule based on consent and indivEIF data.
</p>
</div>
</div>
<div id="footnotes">
<h2 class="footnotes">Footnotes: </h2>
<div id="text-footnotes">

<div class="footdef"><sup><a id="fn.1" class="footnum" href="#fnr.1" role="doc-backlink">1</a></sup> <div class="footpara" role="doc-footnote"><p class="footpara">See the Khan Academy lesson on <a href="https://www.khanacademy.org/computing/ap-computer-science-principles/algorithms-101">algorithms</a> for a general introduction.</p></div></div>

<div class="footdef"><sup><a id="fn.2" class="footnum" href="#fnr.2" role="doc-backlink">2</a></sup> <div class="footpara" role="doc-footnote"><p class="footpara">Runtime efficiency as a differentiating factor between implementations is negligible for such a small task on modern computers. The number of lines of code is mentioned as a criterion because one of the objectives of this example is to show how compact, readable and beginner-friendly code can be.</p></div></div>

<div class="footdef"><sup><a id="fn.3" class="footnum" href="#fnr.3" role="doc-backlink">3</a></sup> <div class="footpara" role="doc-footnote"><p class="footpara">The CSV file format is a lightweight and widely supported tabular data exchange format, and while <a href="https://en.wikipedia.org/wiki/Comma-separated_values">not perfect</a>, is much better than Microsoft Excel files which are still used by some IOs to publish tabular data.</p></div></div>

<div class="footdef"><sup><a id="fn.4" class="footnum" href="#fnr.4" role="doc-backlink">4</a></sup> <div class="footpara" role="doc-footnote"><p class="footpara">Yes, code styling is a thing, and this code block actually does not follow the popular <a href="https://www.python.org/dev/peps/pep-0008/">PEP 8</a> style guide for python code.</p></div></div>

<div class="footdef"><sup><a id="fn.5" class="footnum" href="#fnr.5" role="doc-backlink">5</a></sup> <div class="footpara" role="doc-footnote"><p class="footpara">The full path to the csv file would need to be given if the script is run from a different folder.</p></div></div>

<div class="footdef"><sup><a id="fn.6" class="footnum" href="#fnr.6" role="doc-backlink">6</a></sup> <div class="footpara" role="doc-footnote"><p class="footpara">Regular expressions would be much more suited for accurate pattern matching and information extraction from treaties, see e.g. code examples discussed in my treaty reference automation <a href="https://gitlab.com/martinakunz/treaty-references">project</a>.</p></div></div>


</div>
</div></div>
<div id="postamble" class="status">
<footer>
<div class='container'><div class='item'>
Copyright © 2020–2023 CC-BY 4.0 Martina Kunz<br>
Last updated 2023-02-22 <br>
Built with <a href="https://www.gnu.org/software/emacs/">Emacs</a> 27.1 (<a href="https://orgmode.org">Orgmode</a> 9.5) <br>
Source code shared under <a href='https://www.gnu.org/licenses/agpl-3.0.html'>AGPL</a> on <a href='https://gitlab.com/martinakunz/globalaigov'>Gitlab</a> <br></div>
<div class='item'><a href='https://gitlab.com/martinakunz/globalaigov'><img src='/img/gitlab-logo-700.png' height='90' class='logo'/></a>
<a href='https:/d3js.org'><img src='/img/d3-white.png' height='50' class='logo'/></a></div>
</div>
<p> The information provided by this website is for general informational and educational purposes only and is not a substitute for professional legal advice. It is work in progress and necessarily incomplete.
State borders and denominations do not imply endorsement. Maps use <a href='https://www.naturalearthdata.com/downloads/50m-cultural-vectors/50m-admin-0-details/'>Natural Earth</a> data. More info <a href='https://gitlab.com/legalinformatics/treaty-participation-maps'>here</a>.</p>
<p> Work on this website was supported by Cambridge University's Leverhulme Centre for the Future of Intelligence (<a href='http://lcfi.ac.uk/'>LCFI</a>), under Leverhulme Trust Grant RC-2015-067.</p>
</footer>
</div>
</body>
</html>
