<!DOCTYPE html>
<html lang="en">
<head>
<!-- 2023-02-22 Wed 17:22 -->
<meta charset="utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1" />
<title>AI and international organizations</title>
<meta name="author" content="Martina Kunz" />
<meta name="generator" content="Org Mode" />
<link rel="stylesheet" href="/css/aigov.css" />
<link rel="icon" type="image/png" href="/img/favicon.png">
<script src="/js/d3.v7.min.js"></script>
<script src="/js/d3-geo.v3.min.js"></script>
<script>window.addEventListener('load', function addfntooltips() {var refs = document.getElementsByClassName("footref"); var defs = document.querySelectorAll("p.footpara"); var i, strl = []; for (i = 0; i < refs.length; i++) {strl[i] = defs[i].innerHTML; refs[i].insertAdjacentHTML("beforeend","<span class='fntooltip'>" + strl[i] + "</span>");}}, false);</script>
<script> window.goatcounter={path: function(p) {return location.host + p}} </script>
<script data-goatcounter="https://ahuman.goatcounter.com/count" async src="//gc.zgo.at/count.js"></script>
<noscript> <img src="https://ahuman.goatcounter.com/count?p=/test-noscript"></noscript>
</head>
<body>
<div id="org-div-home-and-up">
 <a accesskey="h" href=""> UP </a>
 |
 <a accesskey="H" href="/"> HOME </a>
</div><div id="preamble" class="status">
<header>
<h1>Global AI Governance</h1>
</header>
<nav>
<ul>
<li><a href='/'>Home</a></li>
<li><a href='/'>Themes</a>
<ul>
<li><a href='/themes/transport.html'>AI & Transport</a>
<ul>
<li><a href='/themes/aviation.html'>Aviation</a></li>
<li><a href='/themes/shipping.html'>Shipping</a></li>
<li><a href='/themes/groundtransport.html'>Ground Transport</a></li>
</ul>
</li>
<li><a href='/themes/crime.html'>AI & Crime</a></li>
</ul>
</li>
<li><a href='/'>Tools</a>
<ul>
<li><a href='/tools/treaties/'>Treaties</a></li>
<li><a href='/tools/standards.html'>Voluntary Standards</a></li>
<li><a href='/tools/globalgoals.html'>Global Goals</a></li>
</ul>
</li>
<li><a href='/participants/'>Participants</a>
<ul>
<li><a href='/participants/states.html'>States</a>
<ul>
<li><a href='/participants/china.html'>China</a></li>
<li><a href='/participants/usa.html'>United States</a></li>
</ul>
</li>
<li><a href='/participants/igos.html'>Intergovernmental Organizations</a></li>
<li><a href='/participants/fora.html'>Multistakeholder Fora</a></li>
</ul>
</li>
</ul>
</nav>
</div>
<div id="content" class="content">
<nav id="table-of-contents" role="doc-toc">
<h2>Table of Contents</h2>
<div id="text-table-of-contents" role="doc-toc">
<ul>
<li><a href="#org6a69ce8">International organizations and AI</a>
<ul>
<li><a href="#orgdadb2ec">IOs and other orgs</a></li>
<li><a href="#org2240842">The UN System</a></li>
<li><a href="#org1f7a748">AI-related activities at global IOs</a></li>
</ul>
</li>
</ul>
</div>
</nav>

<div id="outline-container-org6a69ce8" class="outline-2">
<h2 id="org6a69ce8">International organizations and AI</h2>
<div class="outline-text-2" id="text-org6a69ce8">
</div>
<div id="outline-container-orgdadb2ec" class="outline-3">
<h3 id="orgdadb2ec">IOs and other orgs</h3>
<div class="outline-text-3" id="text-orgdadb2ec">
<p>
First of all, the term &ldquo;international organization&rdquo; (IO) is used here in the sense defined by the 1969 Vienna Convention on the Law of Treaties:<sup><a id="fnr.1" class="footref" href="#fn.1" role="doc-backlink">1</a></sup>
</p>

<blockquote>
<p>
art 2(1)(i) &ldquo;international organization&rdquo; means an intergovernmental organization. 
</p>
</blockquote>

<p>
Names can be misleading. For example, the International Organization for Standardization (<a href="https://www.iso.org">ISO</a>) is not an IO in this sense, it is a non-governmental organization.<sup><a id="fnr.2" class="footref" href="#fn.2" role="doc-backlink">2</a></sup> Conversely, the International Telecommunications Union (<a href="https://www.itu.int">ITU</a>) does not have &ldquo;organization&rdquo; in its name, but it is an IO in the sense defined above.
</p>

<p>
Sometimes the acronyms IGO and INGO (international non-governmental organization) are used to clarify the type of organization considered. However, from a linguistic and legal perspective, &ldquo;transnational&rdquo; (active across nation-states) or &ldquo;multinational&rdquo; (active in multiple nation-states) makes a bit more sense than &ldquo;international&rdquo; (between nation-states), when referring to non-governmental entities. In practice problems only arise when the use of adjectives like &ldquo;international&rdquo; lead to confusions regarding regulatory authority or applicability of international law.
</p>

<p>
The term &ldquo;global&rdquo; IO/IGO/organization is used here for IOs which have at least 75% of the world&rsquo;s states as members. The exact number of states existing at any given time is contested, but the UN currently lists <a href="https://www.un.org/en/about-us/growth-in-un-membership">193</a> member states, so the threshold for a &ldquo;global&rdquo; IO in this sense would be 145 members.<sup><a id="fnr.3" class="footref" href="#fn.3" role="doc-backlink">3</a></sup>
</p>
</div>
</div>

<div id="outline-container-org2240842" class="outline-3">
<h3 id="org2240842">The UN System</h3>
<div class="outline-text-3" id="text-org2240842">

<figure id="org6694a0d">
<img src="../img/un_system_chart.jpg" alt="un_system_chart.jpg">

</figure>

<p>
The latest pdf version of this chart can be found at this <a href="https://www.un.org/en/pdfs/un_system_chart.pdf">location</a>. It is available in all six UN languages.
</p>

<p>
As this organizational chart shows, almost all global IOs are within the UN system in some form or another. Thus the concept of the &ldquo;UN system&rdquo; (sometimes also called &ldquo;UN family&rdquo;) is much broader than the UN itself, which is the organization set up by the <a href="https://www.un.org/en/about-us/un-charter">UN Charter</a>. Incidentally, <a href="https://www.irena.org">IRENA</a>, the International Renewable Energy Agency founded in 2009, is an example of a global intergovernmental organization which is not (yet) within the UN system.
</p>
</div>
</div>

<div id="outline-container-org1f7a748" class="outline-3">
<h3 id="org1f7a748">AI-related activities at global IOs</h3>
<div class="outline-text-3" id="text-org1f7a748">
<p>
The UN System Chief Executives Board for Coordination (<a href="https://unsceb.org">CEB</a>) has been <a href="https://unsceb.org/topics/artificial-intelligence">examining</a> the risks and opportunities of AI for the UN system and its member states since 2017 and developed a UN system-wide <a href="https://unsceb.org/united-nations-system-wide-strategic-approach-and-road-map-supporting-capacity-development">strategy and roadmap</a> for AI capacity-building in 2019. In this strategy, the main paragraph on AI governance states (footnotes omitted):
</p>

<blockquote>
<p>
12.(e) <b>Policy, law, and human rights.</b> Existing frameworks for policy, law and human rights may not adequately address the impacts of artificial intelligence that will be faced in the near future. A combination of good oversight and, perhaps, new normative standards may be needed to achieve fair, transparent and accountable algorithmic decision processes, prevent discrimination and bias and, as the Global Commission on the Future of Work recommends in its report, implement a “human-in-command” approach that ensures the final decisions affecting work are taken by human beings, not algorithms. Building on a long tradition of promoting human and workers’ rights, the United Nations system can help Member States, especially developing countries, to have guardrails that leave open opportunities for experimentation and innovation, while protecting vulnerable populations and workers in newly emerging digital platform-based economies. The United Nations system can even consider how emerging technologies can be leveraged to advance human rights and gender equality, and to help to formalize work and training in the informal economy. There should be an improvement to the capacity of developing countries to design and implement innovation policies.
</p>
</blockquote>

<p>
In 2020, an inter-agency working group on AI was established to build on this and the AI ethics workstream, co-led by UNESCO and ITU. UNESCO adopted a <a href="https://unesdoc.unesco.org/ark:/48223/pf0000381137">Recommendation on the Ethics of AI</a> in 2021.
</p>

<p>
Some of the AI-related activities are primarily about applying AI rather than governing it, but any IO using such technologies needs to have internal rules and guidelines on procurement, data protection, security etc. depending on the type of application.
</p>

<p>
Thematic pages introduce the work of relevant organizations, such as <a href="https://www.icao.int/">ICAO&rsquo;s</a> work on <a href="../themes/aviation.html">AI and aviation</a>, and the <a href="https://www.imo.org/">IMO&rsquo;s</a> governance efforts on <a href="../themes/shipping.html">autonomous shipping</a>.
</p>
</div>
</div>
</div>
<div id="footnotes">
<h2 class="footnotes">Footnotes: </h2>
<div id="text-footnotes">

<div class="footdef"><sup><a id="fn.1" class="footnum" href="#fnr.1" role="doc-backlink">1</a></sup> <div class="footpara" role="doc-footnote"><p class="footpara">Vienna Convention on the Law of Treaties, adopted 23 May 1969, entered into force 27 January 1980, 1155 UNTS 331. <a href="https://legal.un.org/ilc/texts/instruments/english/conventions/1_1_1969.pdf">(pdf)</a></p></div></div>

<div class="footdef"><sup><a id="fn.2" class="footnum" href="#fnr.2" role="doc-backlink">2</a></sup> <div class="footpara" role="doc-footnote"><p class="footpara">More specifically, ISO is a private association under Swiss Law, see Art 22.1 of its <a href="http://www.iso.org/iso/statutes.pdf">Statutes</a> (&ldquo;The Organization shall be a member-based association with civil personality in accordance with Article 60 et seq. of the Swiss Civil Code. It shall be not-for-profit and non-governmental.&rdquo;)</p></div></div>

<div class="footdef"><sup><a id="fn.3" class="footnum" href="#fnr.3" role="doc-backlink">3</a></sup> <div class="footpara" role="doc-footnote"><p class="footpara">&ldquo;Universal&rdquo; would have been more aligned with established practice (as in &ldquo;universal ratification&rdquo;, or as used in the 1975 Vienna Convention on the Representation of States in their Relations
with International Organizations of a Universal Character), but representing the Universe seems a tad overambitious at our current state of technological development.</p></div></div>


</div>
</div></div>
<div id="postamble" class="status">
<footer>
<div class='container'><div class='item'>
Copyright © 2020–2023 CC-BY 4.0 Martina Kunz<br>
Last updated 2023-02-22 <br>
Built with <a href="https://www.gnu.org/software/emacs/">Emacs</a> 27.1 (<a href="https://orgmode.org">Orgmode</a> 9.5) <br>
Source code shared under <a href='https://www.gnu.org/licenses/agpl-3.0.html'>AGPL</a> on <a href='https://gitlab.com/martinakunz/globalaigov'>Gitlab</a> <br></div>
<div class='item'><a href='https://gitlab.com/martinakunz/globalaigov'><img src='/img/gitlab-logo-700.png' height='90' class='logo'/></a>
<a href='https:/d3js.org'><img src='/img/d3-white.png' height='50' class='logo'/></a></div>
</div>
<p> The information provided by this website is for general informational and educational purposes only and is not a substitute for professional legal advice. It is work in progress and necessarily incomplete.
State borders and denominations do not imply endorsement. Maps use <a href='https://www.naturalearthdata.com/downloads/50m-cultural-vectors/50m-admin-0-details/'>Natural Earth</a> data. More info <a href='https://gitlab.com/legalinformatics/treaty-participation-maps'>here</a>.</p>
<p> Work on this website was supported by Cambridge University's Leverhulme Centre for the Future of Intelligence (<a href='http://lcfi.ac.uk/'>LCFI</a>), under Leverhulme Trust Grant RC-2015-067.</p>
</footer>
</div>
</body>
</html>
