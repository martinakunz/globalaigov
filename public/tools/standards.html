<!DOCTYPE html>
<html lang="en">
<head>
<!-- 2023-02-22 Wed 17:22 -->
<meta charset="utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1" />
<title>Standards for AI &amp; Robotics</title>
<meta name="author" content="Martina Kunz" />
<meta name="generator" content="Org Mode" />
<link rel="stylesheet" href="/css/aigov.css" />
<link rel="icon" type="image/png" href="/img/favicon.png">
<script src="/js/d3.v7.min.js"></script>
<script src="/js/d3-geo.v3.min.js"></script>
<script>window.addEventListener('load', function addfntooltips() {var refs = document.getElementsByClassName("footref"); var defs = document.querySelectorAll("p.footpara"); var i, strl = []; for (i = 0; i < refs.length; i++) {strl[i] = defs[i].innerHTML; refs[i].insertAdjacentHTML("beforeend","<span class='fntooltip'>" + strl[i] + "</span>");}}, false);</script>
<script> window.goatcounter={path: function(p) {return location.host + p}} </script>
<script data-goatcounter="https://ahuman.goatcounter.com/count" async src="//gc.zgo.at/count.js"></script>
<noscript> <img src="https://ahuman.goatcounter.com/count?p=/test-noscript"></noscript>
</head>
<body>
<div id="org-div-home-and-up">
 <a accesskey="h" href=""> UP </a>
 |
 <a accesskey="H" href="/"> HOME </a>
</div><div id="preamble" class="status">
<header>
<h1>Global AI Governance</h1>
</header>
<nav>
<ul>
<li><a href='/'>Home</a></li>
<li><a href='/'>Themes</a>
<ul>
<li><a href='/themes/transport.html'>AI & Transport</a>
<ul>
<li><a href='/themes/aviation.html'>Aviation</a></li>
<li><a href='/themes/shipping.html'>Shipping</a></li>
<li><a href='/themes/groundtransport.html'>Ground Transport</a></li>
</ul>
</li>
<li><a href='/themes/crime.html'>AI & Crime</a></li>
</ul>
</li>
<li><a href='/'>Tools</a>
<ul>
<li><a href='/tools/treaties/'>Treaties</a></li>
<li><a href='/tools/standards.html'>Voluntary Standards</a></li>
<li><a href='/tools/globalgoals.html'>Global Goals</a></li>
</ul>
</li>
<li><a href='/participants/'>Participants</a>
<ul>
<li><a href='/participants/states.html'>States</a>
<ul>
<li><a href='/participants/china.html'>China</a></li>
<li><a href='/participants/usa.html'>United States</a></li>
</ul>
</li>
<li><a href='/participants/igos.html'>Intergovernmental Organizations</a></li>
<li><a href='/participants/fora.html'>Multistakeholder Fora</a></li>
</ul>
</li>
</ul>
</nav>
</div>
<div id="content" class="content">
<nav id="table-of-contents" role="doc-toc">
<h2>Table of Contents</h2>
<div id="text-table-of-contents" role="doc-toc">
<ul>
<li><a href="#orgb5898d7">Voluntary technical standards for AI and robotics</a></li>
<li><a href="#orgf9f1d37">ISO and IEC</a>
<ul>
<li><a href="#org2a1eaa0">Robotics safety standards</a></li>
<li><a href="#orga7a8b79">AI/ML standardization</a></li>
</ul>
</li>
<li><a href="#orgc793c3d">ITU</a></li>
<li><a href="#orgd598ec1">IEEE</a></li>
</ul>
</div>
</nav>

<div id="outline-container-orgb5898d7" class="outline-2">
<h2 id="orgb5898d7">Voluntary technical standards for AI and robotics</h2>
<div class="outline-text-2" id="text-orgb5898d7">
<p>
In this context, the term &ldquo;standards&rdquo; refers to norms such as those adopted by the International Organization for Standardization (<a href="https://www.iso.org">ISO</a>) which are non-binding per se but which can become binding when referred to as such by national or international law.
</p>

<p>
Apart from economic incentives to develop and comply with international standards in a globalized economy, the 164 members of the World Trade Organization (WTO) are required by the WTO <a href="https://www.wto.org/english/docs_e/legal_e/17-tbt_e.htm">Agreement on Technical Barriers to Trade</a><sup><a id="fnr.1" class="footref" href="#fn.1" role="doc-backlink">1</a></sup> to use relevant international standards as a basis for their (binding) technical regulations at the national level, &ldquo;except when such international standards or relevant parts would be an ineffective or inappropriate means for the fulfilment of the legitimate objectives pursued&rdquo; (art 2.4). The flexibility accorded by this provision with regard to the choice of standard and manner of its use has been the subject of many debates. These debates are particularly salient in the domain of ICTs, where private standardization bodies have come to challenge the monopoly of the traditional trio of ISO, the International Electrotechnical Commission (<a href="https://www.iec.ch/">IEC</a>) and the International Telecommunications Union (<a href="https://www.itu.int/">ITU</a>).
</p>
</div>
</div>

<div id="outline-container-orgf9f1d37" class="outline-2">
<h2 id="orgf9f1d37">ISO and IEC</h2>
<div class="outline-text-2" id="text-orgf9f1d37">
<p>
Aside from introductory sections, ISO and IEC standards are not freely available and subject to strict copyright, thus unsuitable for detailed discussion on this website. The following is only a brief overview.
</p>
</div>

<div id="outline-container-org2a1eaa0" class="outline-3">
<h3 id="org2a1eaa0">Robotics safety standards</h3>
<div class="outline-text-3" id="text-org2a1eaa0">
<p>
ISO’s first industrial robotics safety standard was <a href="https://www.iso.org/standard/18252.html">ISO 10218:1992</a> ‘Manipulating industrial robots – Safety’, which subsequently expanded in scope and from 2006 on was published in two parts, of which the most recent versions are <a href="https://www.iso.org/standard/51330.html">ISO 10218-1:2011</a> ‘Robots and robotic devices – Safety requirements for industrial robots – Part 1: Robots’ and <a href="https://www.iso.org/standard/41571.html">ISO 10218-2:2011</a> ‘Robots and robotic devices – Safety requirements for industrial robots – Part 2: Robot systems and integration’.
</p>

<p>
The 2011 version of the two-part ISO 10218 standard already includes guidelines for collaborative applications, but is currently being revised to incorporate supplementary guidance from technical specification <a href="https://www.iso.org/standard/62996.html">ISO/TS 15066:2016</a> ‘Robots and robotic devices – Collaborative robots’ as well as other developments.
</p>

<p>
As for service robot safety, the principal standard published thus far by the ISO Technical Committee on Robotics (<a href="https://committee.iso.org/home/tc299">ISO/TC 299</a>) is <a href="https://www.iso.org/standard/53820.html">ISO 13482:2014</a> ‘Robots and robotic devices – Safety requirements for personal care robots’, with more underway.
</p>

<p>
A joint working group with IEC also published two medical robot safety standards: 
</p>
<ul class="org-ul">
<li><a href="https://www.iso.org/standard/68473.html">IEC 80601-2-77:2019</a> on basic safety and essential performance of robotically assisted surgical equipment</li>
<li><a href="https://www.iso.org/standard/68474.html">IEC 80601-2-78:2019</a> on basic safety and essential performance of medical robots for rehabilitation, assessment, compensation or alleviation</li>
</ul>
</div>
</div>

<div id="outline-container-orga7a8b79" class="outline-3">
<h3 id="orga7a8b79">AI/ML standardization</h3>
<div class="outline-text-3" id="text-orga7a8b79">
<p>
Attempts to standardize AI/ML at ISO date back to at least the 1990s but only reached the stage of defining basic vocabulary. In 2017 these efforts were revived by establishing a new subcommittee on AI under the Joint Technical Committee 1 (on IT) of ISO and IEC, the <a href="https://www.iso.org/committee/6794475.html">ISO/IEC JTC 1/SC 42</a>.
</p>

<p>
Most of the outputs so far are of a non-normative <a href="https://www.iso.org/deliverables-all.html">type</a>, such as technical reports, or limited to vocabulary and conceptual frameworks. An exception is <a href="https://www.iso.org/standard/56641.html">ISO/IEC 38507:2022</a> on governance implications of the use of AI by organizations.
</p>
</div>
</div>
</div>


<div id="outline-container-orgc793c3d" class="outline-2">
<h2 id="orgc793c3d">ITU</h2>
<div class="outline-text-2" id="text-orgc793c3d">
<p>
The ITU being an intergovernmental organization, most of its standards are freely available. <a href="https://www.itu.int/en/ITU-T/publications/Pages/recs.aspx">ITU-T Recommendations</a> are the main non-binding standardization output of ITU, with over 4000 in force to date across a wide range of topics.
</p>

<p>
Some examples of relevant Recommendations in force:
</p>
<ul class="org-ul">
<li><a href="https://www.itu.int/ITU-T/recommendations/rec.aspx?rec=14329&amp;lang=en">ITU-T F.748.11 (08/2020)</a> : Metrics and evaluation methods for a deep neural network processor benchmark</li>
<li><a href="https://www.itu.int/ITU-T/recommendations/rec.aspx?rec=14405&amp;lang=en">ITU-T Y.3531 (09/2020)</a> : Cloud computing - Functional requirements for machine learning as a service</li>
<li><a href="https://www.itu.int/ITU-T/recommendations/rec.aspx?rec=13916&amp;lang=en">ITU-T F.746.9 (05/2019)</a> : Requirements and architecture for indoor conversational robot systems</li>
</ul>
</div>
</div>


<div id="outline-container-orgd598ec1" class="outline-2">
<h2 id="orgd598ec1">IEEE</h2>
<div class="outline-text-2" id="text-orgd598ec1">
<p>
The Institute of Electrical and Electronics Engineers (<a href="https://www.ieee.org">IEEE</a>) Standards Association has a number of AI systems standardization <a href="https://standards.ieee.org/initiatives/artificial-intelligence-systems/standards/">projects</a> ongoing, but drafts and further information are not freely accessible.
</p>
</div>
</div>
<div id="footnotes">
<h2 class="footnotes">Footnotes: </h2>
<div id="text-footnotes">

<div class="footdef"><sup><a id="fn.1" class="footnum" href="#fnr.1" role="doc-backlink">1</a></sup> <div class="footpara" role="doc-footnote"><p class="footpara">Agreement on Technical Barriers to Trade (adopted 15 April 1994, entered into force 1 January 1995) 1868 UNTS 120 (TBT).</p></div></div>


</div>
</div></div>
<div id="postamble" class="status">
<footer>
<div class='container'><div class='item'>
Copyright © 2020–2023 CC-BY 4.0 Martina Kunz<br>
Last updated 2023-02-22 <br>
Built with <a href="https://www.gnu.org/software/emacs/">Emacs</a> 27.1 (<a href="https://orgmode.org">Orgmode</a> 9.5) <br>
Source code shared under <a href='https://www.gnu.org/licenses/agpl-3.0.html'>AGPL</a> on <a href='https://gitlab.com/martinakunz/globalaigov'>Gitlab</a> <br></div>
<div class='item'><a href='https://gitlab.com/martinakunz/globalaigov'><img src='/img/gitlab-logo-700.png' height='90' class='logo'/></a>
<a href='https:/d3js.org'><img src='/img/d3-white.png' height='50' class='logo'/></a></div>
</div>
<p> The information provided by this website is for general informational and educational purposes only and is not a substitute for professional legal advice. It is work in progress and necessarily incomplete.
State borders and denominations do not imply endorsement. Maps use <a href='https://www.naturalearthdata.com/downloads/50m-cultural-vectors/50m-admin-0-details/'>Natural Earth</a> data. More info <a href='https://gitlab.com/legalinformatics/treaty-participation-maps'>here</a>.</p>
<p> Work on this website was supported by Cambridge University's Leverhulme Centre for the Future of Intelligence (<a href='http://lcfi.ac.uk/'>LCFI</a>), under Leverhulme Trust Grant RC-2015-067.</p>
</footer>
</div>
</body>
</html>
