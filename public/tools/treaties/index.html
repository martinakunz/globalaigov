<!DOCTYPE html>
<html lang="en">
<head>
<!-- 2023-02-22 Wed 17:22 -->
<meta charset="utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1" />
<title>Treaties and global AI governance</title>
<meta name="author" content="Martina Kunz" />
<meta name="generator" content="Org Mode" />
<link rel="stylesheet" href="/css/aigov.css" />
<link rel="icon" type="image/png" href="/img/favicon.png">
<script src="/js/d3.v7.min.js"></script>
<script src="/js/d3-geo.v3.min.js"></script>
<script>window.addEventListener('load', function addfntooltips() {var refs = document.getElementsByClassName("footref"); var defs = document.querySelectorAll("p.footpara"); var i, strl = []; for (i = 0; i < refs.length; i++) {strl[i] = defs[i].innerHTML; refs[i].insertAdjacentHTML("beforeend","<span class='fntooltip'>" + strl[i] + "</span>");}}, false);</script>
<script> window.goatcounter={path: function(p) {return location.host + p}} </script>
<script data-goatcounter="https://ahuman.goatcounter.com/count" async src="//gc.zgo.at/count.js"></script>
<noscript> <img src="https://ahuman.goatcounter.com/count?p=/test-noscript"></noscript>
</head>
<body>
<div id="org-div-home-and-up">
 <a accesskey="h" href=""> UP </a>
 |
 <a accesskey="H" href="/"> HOME </a>
</div><div id="preamble" class="status">
<header>
<h1>Global AI Governance</h1>
</header>
<nav>
<ul>
<li><a href='/'>Home</a></li>
<li><a href='/'>Themes</a>
<ul>
<li><a href='/themes/transport.html'>AI & Transport</a>
<ul>
<li><a href='/themes/aviation.html'>Aviation</a></li>
<li><a href='/themes/shipping.html'>Shipping</a></li>
<li><a href='/themes/groundtransport.html'>Ground Transport</a></li>
</ul>
</li>
<li><a href='/themes/crime.html'>AI & Crime</a></li>
</ul>
</li>
<li><a href='/'>Tools</a>
<ul>
<li><a href='/tools/treaties/'>Treaties</a></li>
<li><a href='/tools/standards.html'>Voluntary Standards</a></li>
<li><a href='/tools/globalgoals.html'>Global Goals</a></li>
</ul>
</li>
<li><a href='/participants/'>Participants</a>
<ul>
<li><a href='/participants/states.html'>States</a>
<ul>
<li><a href='/participants/china.html'>China</a></li>
<li><a href='/participants/usa.html'>United States</a></li>
</ul>
</li>
<li><a href='/participants/igos.html'>Intergovernmental Organizations</a></li>
<li><a href='/participants/fora.html'>Multistakeholder Fora</a></li>
</ul>
</li>
</ul>
</nav>
</div>
<div id="content" class="content">
<nav id="table-of-contents" role="doc-toc">
<h2>Table of Contents</h2>
<div id="text-table-of-contents" role="doc-toc">
<ul>
<li><a href="#orgda5cff2">Treaties as a tool for global AI governance</a>
<ul>
<li><a href="#org199c000">Terminology</a></li>
<li><a href="#org9ade32b">Treaty life cycle</a></li>
<li><a href="#orgfdda2db">Treaties vs other tools</a></li>
</ul>
</li>
<li><a href="#orgfb23983">List of relevant treaties</a></li>
</ul>
</div>
</nav>

<div id="outline-container-orgda5cff2" class="outline-2">
<h2 id="orgda5cff2">Treaties as a tool for global AI governance</h2>
<div class="outline-text-2" id="text-orgda5cff2">
</div>
<div id="outline-container-org199c000" class="outline-3">
<h3 id="org199c000">Terminology</h3>
<div class="outline-text-3" id="text-org199c000">
<p>
First of all, the term &ldquo;treaty&rdquo; is used here in the sense defined by the <a href="http://legal.un.org/ilc/texts/instruments/english/conventions/1_1_1969.pdf">1969 Vienna Convention on the Law of Treaties</a> (VCLT)<sup><a id="fnr.1" class="footref" href="#fn.1" role="doc-backlink">1</a></sup>:
</p>

<blockquote>
<dl class="org-dl">
<dt>2(1)</dt><dd><dl class="org-dl">
<dt>(a)</dt><dd>“treaty” means an international agreement concluded between States in written form and governed by international law, whether embodied in a single instrument or in two or more related instruments and whatever its particular designation</dd>
</dl></dd>
</dl>
</blockquote>

<p>
This means that states can call their agreement &ldquo;convention&rdquo;, &ldquo;charter&rdquo;, &ldquo;pact&rdquo;, &ldquo;protocol&rdquo; or whatever they like &#x2014; legally speaking there is no difference. As a matter of social convention, &ldquo;constitution&rdquo; (as in <a href="https://www.who.int/about/governance/constitution">Constitution of the World Health Organization</a>) and &ldquo;charter&rdquo; (as in <a href="https://www.un.org/en/charter-united-nations">Charter of the United Nations</a>) are terms used for more foundational treaties, possibly the constituent instrument of an international organization, but there are counterexamples.
</p>

<p>
Other important terms describe the status of a participant with regard to a treaty:
</p>

<blockquote>
<dl class="org-dl">
<dt>2(1)</dt><dd><dl class="org-dl">
<dt>(e)</dt><dd>“negotiating State” means a State which took part in the drawing up and adoption of the text of the treaty;</dd>
<dt>(f)</dt><dd>“contracting State” means a State which has consented to be bound by the treaty, whether or not the treaty has entered into force;</dd>
<dt>(g)</dt><dd>“party” means a State which has consented to be bound by the treaty and for which the treaty is in force;</dd>
<dt>(h)</dt><dd>“third State” means a State not a party to the treaty;</dd>
</dl></dd>
</dl>
</blockquote>

<p>
These definitions apply <i>mutatis mutandis</i> to supranational entities lawfully participating in treaties, such as the European Union. 
</p>

<p>
What these definitions show is that consent to be bound is a key notion in the Law of Treaties. VCLT article 11 sets out the means of expressing consent to be bound:
</p>

<blockquote>
<dl class="org-dl">
<dt>11</dt><dd>The consent of a State to be bound by a treaty may be expressed by signature, exchange of instruments constituting a treaty, ratification, acceptance, approval or accession, or by any other means if so agreed.</dd>
</dl>
</blockquote>

<p>
Signature can be a means of expressing consent to be bound or it can be subject to ratification. The latter is commonly the case for large-scale multilateral treaties, including those discussed on this website, and hence &ldquo;signatory&rdquo; is <i>not</i> a synonym for &ldquo;party&rdquo;. 
</p>

<p>
For treaty participation maps elsewhere on this website the term &ldquo;contractant&rdquo; is used to denote the intermediary status between signatory and party, i.e. in line with the definitions above, for &ldquo;a State which has consented to be bound by the treaty, but for which the treaty has not yet entered into force&rdquo;. There are two main scenarios: (i) the treaty has not yet reached the conditions for entry into force, or (ii) the treaty has specified a waiting period between expression of consent to be bound and entry into force.
</p>

<p>
Acceptance and approval are essentially the same as ratification, whereas accession is typically reserved to states not having signed the treaty. Whether and under which conditions accession is possible depends on the treaty at hand.
</p>
</div>
</div>

<div id="outline-container-org9ade32b" class="outline-3">
<h3 id="org9ade32b">Treaty life cycle</h3>
<div class="outline-text-3" id="text-org9ade32b">
<p>
There are a few important events and stages in the &ldquo;life&rdquo; of a treaty that are frequently referred to on this website and elsewhere. Here a synopsis for multilateral treaties:
</p>

<ol class="org-ol">
<li>Negotiating and drafting phase (can take years)</li>
<li>Adoption of the final treaty text (also called conclusion)</li>
<li>Signature (often in a ceremony and serving as authentication of the text in its official languages)</li>
<li>Domestic ratification phase (can take years)</li>
<li>Deposit of the instrument of ratification/acceptance/approval with the designated treaty depositary</li>
<li>Once the conditions for entry into force are met (typically depending on the number of ratifications, but sometimes also on additional conditions), the depositary communicates the entry into force to all participants (typically after a waiting period of a few months to allow for preparations)</li>
<li>Entry into force &#x2013; the treaty becomes binding for all its contracting states (but not for mere signatories and third states)</li>
<li>Accession by non-signatories (if provided for by treaty)</li>
<li>(Denunciation/withdrawal of parties &#x2014; rare)</li>
<li>Treaty amendment in accordance with procedure specified by treaty (typically with adoption and ratification of the amending agreement, but simplified procedures are common as well)</li>
<li>(Possible termination of treaty &#x2014; very rare)</li>
</ol>

<p>
Both the treaty itself and each party to it have an entry into force date for the treaty, and they only coincide if the participant has ratified the treaty by the date the entry into force conditions are met.
</p>
</div>
</div>

<div id="outline-container-orgfdda2db" class="outline-3">
<h3 id="orgfdda2db">Treaties vs other tools</h3>
<div class="outline-text-3" id="text-orgfdda2db">
<p>
It is sometimes argued that treaties too rigid and slow to be useful for effective global governance.  
However, this view, if it is not implicitly arguing for a world government or for industry self-regulation, is often based on a misunderstanding of either the concept of a treaty or of the customary law of treaties (to a large extent reflected in the VCLT). The Law of Treaties imposes very few constraints, and there are many examples of flexible and agile treaties. The bottleneck is typically state consent, i.e. domestic politics, not international law.
</p>
</div>
</div>
</div>

<div id="outline-container-orgfb23983" class="outline-2">
<h2 id="orgfb23983">List of relevant treaties</h2>
<div class="outline-text-2" id="text-orgfb23983">
<p>
The following is a chronological list of treaties discussed elsewhere on this website.
</p>

<table>


<colgroup>
<col  class="org-right">

<col  class="org-left">

<col  class="org-right">

<col  class="org-left">
</colgroup>
<thead>
<tr>
<th scope="col" class="org-right">Adoption</th>
<th scope="col" class="org-left">Treaty</th>
<th scope="col" class="org-right">In Force</th>
<th scope="col" class="org-left">UNTS</th>
</tr>
</thead>
<tbody>
<tr>
<td class="org-right">1944-12-07</td>
<td class="org-left">Convention on International Civil Aviation</td>
<td class="org-right">1947-04-04</td>
<td class="org-left"><a href="https://treaties.un.org/Pages/showDetails.aspx?objid=0800000280163d69">UNTS</a></td>
</tr>

<tr>
<td class="org-right">1948-03-06</td>
<td class="org-left">Convention on the International Maritime Organization</td>
<td class="org-right">1958-03-17</td>
<td class="org-left"><a href="https://treaties.un.org/Pages/showDetails.aspx?objid=0800000280022758">UNTS</a></td>
</tr>

<tr>
<td class="org-right">1949-09-19</td>
<td class="org-left">Convention on Road Traffic</td>
<td class="org-right">1952-03-26</td>
<td class="org-left"><a href="https://treaties.un.org/Pages/showDetails.aspx?objid=080000028005793f">UNTS</a></td>
</tr>

<tr>
<td class="org-right">1953-03-31</td>
<td class="org-left">Convention on the International Right of Correction</td>
<td class="org-right">1962-08-24</td>
<td class="org-left"><a href="https://treaties.un.org/Pages/showDetails.aspx?objid=0800000280033202">UNTS</a></td>
</tr>

<tr>
<td class="org-right">1958-03-20</td>
<td class="org-left">Agreement concerning the Adoption of Harmonized Technical United Nations Regulations for Wheeled Vehicles, Equipment and Parts which can be Fitted and/or be Used on Wheeled Vehicles and the Conditions for Reciprocal Recognition of Approvals Granted on the Basis of these United Nations Regulations</td>
<td class="org-right">1959-06-20</td>
<td class="org-left"><a href="https://treaties.un.org/Pages/showDetails.aspx?objid=0800000280009508">UNTS</a></td>
</tr>

<tr>
<td class="org-right">1960-12-15</td>
<td class="org-left">Convention against discrimination in education</td>
<td class="org-right">1962-05-22</td>
<td class="org-left"><a href="https://treaties.un.org/Pages/showDetails.aspx?objid=0800000280134150">UNTS</a></td>
</tr>

<tr>
<td class="org-right">1963-06-25</td>
<td class="org-left">Convention (No. 119) concerning the guarding of machinery</td>
<td class="org-right">1965-04-21</td>
<td class="org-left"><a href="https://treaties.un.org/Pages/showDetails.aspx?objid=080000028012dbbc">UNTS</a></td>
</tr>

<tr>
<td class="org-right">1966-03-07</td>
<td class="org-left">International Convention on the Elimination of All Forms of Racial Discrimination</td>
<td class="org-right">1969-01-04</td>
<td class="org-left"><a href="https://treaties.un.org/Pages/showDetails.aspx?objid=0800000280008954">UNTS</a></td>
</tr>

<tr>
<td class="org-right">1966-12-16</td>
<td class="org-left">International Covenant on Civil and Political Rights</td>
<td class="org-right">1976-03-23</td>
<td class="org-left"><a href="https://treaties.un.org/Pages/showDetails.aspx?objid=0800000280004bf5">UNTS</a></td>
</tr>

<tr>
<td class="org-right">1966-12-16</td>
<td class="org-left">International Covenant on Economic, Social and Cultural Rights</td>
<td class="org-right">1976-01-03</td>
<td class="org-left"><a href="https://treaties.un.org/Pages/showDetails.aspx?objid=080000028002b6ed">UNTS</a></td>
</tr>

<tr>
<td class="org-right">1966-12-16</td>
<td class="org-left">Optional Protocol to the International Covenant on Civil and Political Rights</td>
<td class="org-right">1976-03-23</td>
<td class="org-left"><a href="https://treaties.un.org/Pages/showDetails.aspx?objid=080000028000680a">UNTS</a></td>
</tr>

<tr>
<td class="org-right">1968-11-08</td>
<td class="org-left">Convention on Road Traffic</td>
<td class="org-right">1977-05-21</td>
<td class="org-left"><a href="https://treaties.un.org/Pages/showDetails.aspx?objid=080000028003745e">UNTS</a></td>
</tr>

<tr>
<td class="org-right">1970-12-16</td>
<td class="org-left">Convention for the Suppression of unlawful seizure of aircraft</td>
<td class="org-right">1971-10-14</td>
<td class="org-left"><a href="https://treaties.un.org/Pages/showDetails.aspx?objid=0800000280112834">UNTS</a></td>
</tr>

<tr>
<td class="org-right">1971-09-23</td>
<td class="org-left">Convention for the suppression of unlawful acts against the safety of civil aviation</td>
<td class="org-right">1973-01-26</td>
<td class="org-left"><a href="https://treaties.un.org/Pages/showDetails.aspx?objid=0800000280107e32">UNTS</a></td>
</tr>

<tr>
<td class="org-right">1974-11-01</td>
<td class="org-left">International Convention for the Safety of Life at Sea, 1974</td>
<td class="org-right">1980-05-25</td>
<td class="org-left"><a href="https://treaties.un.org/Pages/showDetails.aspx?objid=08000002800ec37f">UNTS</a></td>
</tr>

<tr>
<td class="org-right">1979-12-18</td>
<td class="org-left">Convention on the Elimination of All Forms of Discrimination against Women</td>
<td class="org-right">1981-09-03</td>
<td class="org-left"><a href="https://treaties.un.org/Pages/showDetails.aspx?objid=080000028000309d">UNTS</a></td>
</tr>

<tr>
<td class="org-right">1980-10-10</td>
<td class="org-left">Convention on Prohibitions or Restrictions on the Use of Certain Conventional Weapons which may be deemed to be Excessively Injurious or to have Indiscriminate Effects (with Protocols I, II and III)</td>
<td class="org-right">1983-12-02</td>
<td class="org-left"><a href="https://treaties.un.org/Pages/showDetails.aspx?objid=080000028003a6cf">UNTS</a></td>
</tr>

<tr>
<td class="org-right">1981-01-28</td>
<td class="org-left">Convention for the protection of individuals with regard to automatic processing of personal data</td>
<td class="org-right">1985-10-01</td>
<td class="org-left"><a href="https://treaties.un.org/Pages/showDetails.aspx?objid=08000002800c905a">UNTS</a></td>
</tr>

<tr>
<td class="org-right">1981-06-22</td>
<td class="org-left">Convention (No. 155) concerning occupational safety and health and the working environment</td>
<td class="org-right">1983-08-11</td>
<td class="org-left"><a href="https://treaties.un.org/Pages/showDetails.aspx?objid=08000002800dbe0a">UNTS</a></td>
</tr>

<tr>
<td class="org-right">1984-12-10</td>
<td class="org-left">Convention against Torture and Other Cruel, Inhuman or Degrading Treatment or Punishment</td>
<td class="org-right">1987-06-26</td>
<td class="org-left"><a href="https://treaties.un.org/Pages/showDetails.aspx?objid=080000028003d679">UNTS</a></td>
</tr>

<tr>
<td class="org-right">1988-03-10</td>
<td class="org-left">Convention for the suppression of unlawful acts against the safety of maritime navigation</td>
<td class="org-right">1992-03-01</td>
<td class="org-left"><a href="https://treaties.un.org/Pages/showDetails.aspx?objid=08000002800b9bd7">UNTS</a></td>
</tr>

<tr>
<td class="org-right">1989-11-20</td>
<td class="org-left">Convention on the Rights of the Child</td>
<td class="org-right">1990-09-02</td>
<td class="org-left"><a href="https://treaties.un.org/Pages/showDetails.aspx?objid=08000002800007fe">UNTS</a></td>
</tr>

<tr>
<td class="org-right">1990-12-18</td>
<td class="org-left">International Convention on the Protection of the Rights of All Migrant Workers and Members of their Families</td>
<td class="org-right">2003-07-01</td>
<td class="org-left"><a href="https://treaties.un.org/Pages/showDetails.aspx?objid=080000028004b0a9">UNTS</a></td>
</tr>

<tr>
<td class="org-right">1992-12-22</td>
<td class="org-left">Constitution and Convention of the International Telecommunication Union</td>
<td class="org-right">1994-07-01</td>
<td class="org-left"><a href="https://treaties.un.org/Pages/showDetails.aspx?objid=08000002800b0730">UNTS</a></td>
</tr>

<tr>
<td class="org-right">1997-12-15</td>
<td class="org-left">International Convention for the Suppression of Terrorist Bombings</td>
<td class="org-right">2001-05-23</td>
<td class="org-left"><a href="https://treaties.un.org/Pages/showDetails.aspx?objid=0800000280048c82">UNTS</a></td>
</tr>

<tr>
<td class="org-right">1998-06-25</td>
<td class="org-left">Agreement concerning the Establishing of Global Technical Regulations for Wheeled Vehicles, Equipment and Parts which can be fitted and/or be used on Wheeled Vehicles</td>
<td class="org-right">2000-08-25</td>
<td class="org-left"><a href="https://treaties.un.org/Pages/showDetails.aspx?objid=080000028004cc60">UNTS</a></td>
</tr>

<tr>
<td class="org-right">1999-10-06</td>
<td class="org-left">Optional Protocol to the Convention on the Elimination of All Forms of Discrimination against Women</td>
<td class="org-right">2000-12-22</td>
<td class="org-left"><a href="https://treaties.un.org/Pages/showDetails.aspx?objid=08000002800030b1">UNTS</a></td>
</tr>

<tr>
<td class="org-right">2000-11-15</td>
<td class="org-left">United Nations Convention against Transnational Organized Crime</td>
<td class="org-right">2003-09-29</td>
<td class="org-left"><a href="https://treaties.un.org/Pages/showDetails.aspx?objid=0800000280050d3e">UNTS</a></td>
</tr>

<tr>
<td class="org-right">2001-11-08</td>
<td class="org-left">Additional Protocol to the Convention for the protection of individuals with regard to automatic processing of personal data, regarding supervisory authorities and transborder data flows</td>
<td class="org-right">2004-07-01</td>
<td class="org-left"><a href="https://treaties.un.org/Pages/showDetails.aspx?objid=0800000280071dc7">UNTS</a></td>
</tr>

<tr>
<td class="org-right">2001-11-23</td>
<td class="org-left">Convention on cybercrime</td>
<td class="org-right">2004-07-01</td>
<td class="org-left"><a href="https://treaties.un.org/Pages/showDetails.aspx?objid=0800000280071e5b">UNTS</a></td>
</tr>

<tr>
<td class="org-right">2002-12-18</td>
<td class="org-left">Optional Protocol to the Convention against Torture and Other Cruel, Inhuman or Degrading Treatment or Punishment</td>
<td class="org-right">2006-06-22</td>
<td class="org-left"><a href="https://treaties.un.org/Pages/showDetails.aspx?objid=080000028003d68b">UNTS</a></td>
</tr>

<tr>
<td class="org-right">2003-01-28</td>
<td class="org-left">Additional Protocol to the Convention on cybercrime, concerning the criminalization of acts of a racist and xenophobic nature committed through computer systems</td>
<td class="org-right">2006-03-01</td>
<td class="org-left"><a href="https://treaties.un.org/Pages/showDetails.aspx?objid=080000028005e487">UNTS</a></td>
</tr>

<tr>
<td class="org-right">2006-12-13</td>
<td class="org-left">Convention on the Rights of Persons with Disabilities</td>
<td class="org-right">2008-05-03</td>
<td class="org-left"><a href="https://treaties.un.org/Pages/showDetails.aspx?objid=080000028017bf87">UNTS</a></td>
</tr>

<tr>
<td class="org-right">2006-12-20</td>
<td class="org-left">International Convention for the Protection of All Persons from Enforced Disappearance</td>
<td class="org-right">2010-12-23</td>
<td class="org-left"><a href="https://treaties.un.org/Pages/showDetails.aspx?objid=0800000280058a5a">UNTS</a></td>
</tr>

<tr>
<td class="org-right">2008-12-10</td>
<td class="org-left">Optional Protocol to the International Covenant on Economic, Social and Cultural Rights</td>
<td class="org-right">2013-05-05</td>
<td class="org-left"><a href="https://treaties.un.org/Pages/showDetails.aspx?objid=0800000280212df4">UNTS</a></td>
</tr>

<tr>
<td class="org-right">2013-04-02</td>
<td class="org-left">Arms Trade Treaty</td>
<td class="org-right">2014-12-24</td>
<td class="org-left"><a href="https://treaties.un.org/Pages/showDetails.aspx?objid=08000002803628c4">UNTS</a></td>
</tr>
</tbody>
</table>

<p>
This list is by no means exhaustive of treaties relevant for international AI governance. Applicable law will depend on the specific circumstances of the use and effects of artificial intelligence technologies.
</p>

<p>
OSCOLA style treaty references for some of these treaties can be downloaded from my <a href="https://gitlab.com/martinakunz/treaty-references">Automating treaty references</a> project repository with explanations for the data processing decisions made and instructions for how to adapt the code.
</p>
</div>
</div>
<div id="footnotes">
<h2 class="footnotes">Footnotes: </h2>
<div id="text-footnotes">

<div class="footdef"><sup><a id="fn.1" class="footnum" href="#fnr.1" role="doc-backlink">1</a></sup> <div class="footpara" role="doc-footnote"><p class="footpara">Vienna Convention on the Law of Treaties (adopted 23 May 1969, in force 27 January 1980) 1155 UNTS 331 (VCLT).</p></div></div>


</div>
</div></div>
<div id="postamble" class="status">
<footer>
<div class='container'><div class='item'>
Copyright © 2020–2023 CC-BY 4.0 Martina Kunz<br>
Last updated 2023-02-22 <br>
Built with <a href="https://www.gnu.org/software/emacs/">Emacs</a> 27.1 (<a href="https://orgmode.org">Orgmode</a> 9.5) <br>
Source code shared under <a href='https://www.gnu.org/licenses/agpl-3.0.html'>AGPL</a> on <a href='https://gitlab.com/martinakunz/globalaigov'>Gitlab</a> <br></div>
<div class='item'><a href='https://gitlab.com/martinakunz/globalaigov'><img src='/img/gitlab-logo-700.png' height='90' class='logo'/></a>
<a href='https:/d3js.org'><img src='/img/d3-white.png' height='50' class='logo'/></a></div>
</div>
<p> The information provided by this website is for general informational and educational purposes only and is not a substitute for professional legal advice. It is work in progress and necessarily incomplete.
State borders and denominations do not imply endorsement. Maps use <a href='https://www.naturalearthdata.com/downloads/50m-cultural-vectors/50m-admin-0-details/'>Natural Earth</a> data. More info <a href='https://gitlab.com/legalinformatics/treaty-participation-maps'>here</a>.</p>
<p> Work on this website was supported by Cambridge University's Leverhulme Centre for the Future of Intelligence (<a href='http://lcfi.ac.uk/'>LCFI</a>), under Leverhulme Trust Grant RC-2015-067.</p>
</footer>
</div>
</body>
</html>
